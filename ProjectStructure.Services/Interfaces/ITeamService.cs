﻿using ProjectStructure.Business.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Interfaces
{
    public interface ITeamService
    {
        void Create(TeamDTO teamDTO);
        IList<TeamDTO> GetAll();
        void Update(TeamDTO teamDTO);
        void Delete(int id);
        IList<Query4DTO> GetTeamsWithUsersOlderThan9();
    }
}
