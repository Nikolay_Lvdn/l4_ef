﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.Business.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public UserService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(UserDTO userDTO)
        {
            if (userDTO == null)
                throw new Exception("Wrong request body");

            var userEntity = _mapper.Map<User>(userDTO);

            userEntity.RegisteredAt = DateTime.Now;
            
            if(!System.Net.Mail.MailAddress.TryCreate(userEntity.Email,out System.Net.Mail.MailAddress result))
                throw new Exception("Invalid Email");

            _unitOfWork.Set<User>().Create(userEntity);
            _unitOfWork.SaveChanges();
        }

        public IList<UserDTO> GetAll()
        {
            return _mapper.Map<IList<User>, IList<UserDTO>>(_unitOfWork.Set<User>().Get());
        }

        public void Update(UserDTO userDTO)
        {
            var userToUpdate = _unitOfWork.Set<User>().Get().FirstOrDefault(user => user.Id == userDTO.Id);

            if (userToUpdate == null)
                throw new Exception("No such user");

            userToUpdate.TeamId = userDTO.TeamId;
            userToUpdate.FirstName = userDTO.FirstName;
            userToUpdate.LastName = userDTO.LastName;
            userToUpdate.Email = userDTO.Email;
            userToUpdate.BirthDay = userDTO.BirthDay;
            _unitOfWork.Set<User>().Update(userToUpdate);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            var userToDelete = _unitOfWork.Set<User>().Get().FirstOrDefault(user => user.Id == id);

            if (userToDelete == null)
                throw new Exception("No such user");

            _unitOfWork.Set<User>().Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IList<Query5DTO> GetOrderedUsersAndTasks()
        {
            var tasks = _unitOfWork.Set<Task>().Get();
            var users = _unitOfWork.Set<User>().Get();

            var result = users.GroupJoin(tasks,
                user => user.Id,
                task => task.PerformerId,
                (user, tasks) => (User: user, Tasks: tasks.OrderByDescending(t => t.Name.Length)))
                .OrderBy(u => u.User.FirstName)
                .ToList();

            if (!result.Any())
                throw new ArgumentException("There are no such teams");

            var resultQuery = result.Select(query5DTO => new Query5DTO
            {
                User = _mapper.Map<UserDTO>(query5DTO.User),
                Tasks = _mapper.Map<IList<Task>, IList<TaskDTO>>(query5DTO.Tasks.ToList())
            }).ToList();

            return resultQuery;
        }

        public Query6DTO GetInfoAboutTasksByUserId(int id)
        {
            var tasks = _unitOfWork.Set<Task>().Get();
            var projects = _unitOfWork.Set<Project>().Get();
            var users = _unitOfWork.Set<User>().Get();

            var result = users.GroupJoin(projects,
                user => user.Id,
                project => project.AuthorId,
                (user, projects) => (User: user, Projects: projects.OrderBy(project => project.CreatedAt)))
                .GroupJoin(tasks,
                data => data.User.Id,
                task => task.PerformerId,
                (data, tasks) => (User: data.User, Projects: data.Projects,
                Tasks: tasks.OrderBy(task => task.FinishedAt != null ? task.FinishedAt - task.CreatedAt : DateTime.Now - task.CreatedAt)))
                .Select(data => (
                    User: data.User,
                    LastUserProject: data.Projects.LastOrDefault(),
                    LastProjectTasksCount: data.Projects.LastOrDefault()?.Tasks?.Count,
                    CountOfIncompleteOrCanceledTasks: data.Tasks?.Count(task => task.FinishedAt == null || task.State == 3),
                    LongestTask: data.Tasks?.LastOrDefault()
                ))
                .FirstOrDefault(data => data.User.Id == id);

            if (result.User == null)
                throw new ArgumentException("There are no such user");

            var resultQuery = new Query6DTO
            {
                User = _mapper.Map<UserDTO>(result.User),
                LastUserProject = _mapper.Map<ProjectDTO>(result.LastUserProject),
                LastProjectTasksCount = result.LastProjectTasksCount,
                CountOfIncompleteOrCanceledTasks = result.CountOfIncompleteOrCanceledTasks,
                LongestTask = _mapper.Map<TaskDTO>(result.LongestTask)
            };

            return resultQuery;
        }
    }
}
