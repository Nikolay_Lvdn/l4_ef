﻿using AutoMapper;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.DataAccess.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectStructure.Business.Services
{
    public class TeamService : ITeamService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public TeamService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public void Create(TeamDTO teamDTO)
        {
            if (teamDTO == null)
                throw new Exception("Wrong request body");

            var teamEntity = _mapper.Map<Team>(teamDTO);

            teamEntity.CreatedAt = DateTime.Now;

            _unitOfWork.Set<Team>().Create(teamEntity);
            _unitOfWork.SaveChanges();
        }

        public IList<TeamDTO> GetAll()
        {
            return _mapper.Map<IList<Team>, IList<TeamDTO>>(_unitOfWork.Set<Team>().Get());
        }

        public void Update(TeamDTO teamDTO)
        {
            var teamToUpdate = _unitOfWork.Set<Team>().Get().FirstOrDefault(team => team.Id == teamDTO.Id);

            if (teamToUpdate == null)
                throw new Exception("No such team");

            teamToUpdate.Name = teamDTO.Name;
            _unitOfWork.Set<Team>().Update(teamToUpdate);
            _unitOfWork.SaveChanges();
        }

        public void Delete(int id)
        {
            var teamToDelete = _unitOfWork.Set<Team>().Get().FirstOrDefault(team => team.Id == id);

            if (teamToDelete == null)
                throw new Exception("No such team");

            _unitOfWork.Set<Team>().Delete(id);
            _unitOfWork.SaveChanges();
        }

        public IList<Query4DTO> GetTeamsWithUsersOlderThan9()
        {
            var teams = _unitOfWork.Set<Team>().Get();
            var users = _unitOfWork.Set<User>().Get();

            var result = teams.GroupJoin(users,
                team => team.Id,
                user => user.TeamId,
                (team, users) =>
                (Id: team.Id, Name: team.Name, Members: users.OrderByDescending(user => user.RegisteredAt)))
                .Where(team => team.Members.All(member => DateTime.Now.Year - member.BirthDay.Year > 10)).ToList();

            if (!result.Any())
                throw new ArgumentException("There are no such teams");

            var resultQuery = result.Select(query4DTO => new Query4DTO
            {
                Id = query4DTO.Id,
                Name = query4DTO.Name,
                Members = _mapper.Map<IList<User>, IList<UserDTO>>(query4DTO.Members.ToList())
            }).ToList();

            return resultQuery;
        }
    }
}
