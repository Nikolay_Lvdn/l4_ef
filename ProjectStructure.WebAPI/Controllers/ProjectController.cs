﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ProjectStructure.Business.DTO;
using ProjectStructure.Business.Interfaces;
using ProjectStructure.WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ProjectStructure.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectService _projectService;
        private readonly IMapper _mapper;

        public ProjectController(IProjectService projectService, IMapper mapper)
        {
            _projectService = projectService;
            _mapper = mapper;
        }

        // GET: api/<ProjectController>
        [HttpGet]
        public ActionResult<IList<GettingProjectView>> Get()
        {
            var projectsDTO = _projectService.GetAll();
            var projectsView = _mapper.Map<IList<ProjectDTO>, IList<GettingProjectView>>(projectsDTO);

            return Ok(projectsView);
        }

        // POST api/<ProjectController>
        [HttpPost]
        public ActionResult Post([FromBody] ProjectView userView)
        {
            try
            {
                var projectDTO = _mapper.Map<ProjectView, ProjectDTO>(userView);

                _projectService.Create(projectDTO);

                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        // PUT api/<ProjectController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] ProjectView projectView)
        {
            try
            {
                var projectDTO = _mapper.Map<ProjectView, ProjectDTO>(projectView);
                projectDTO.Id = id;
                _projectService.Update(projectDTO);

                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // DELETE api/<ProjectController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            try
            {
                _projectService.Delete(id);
                return Ok();
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<ProjectController>/q1
        [HttpGet("q1")]
        public ActionResult<IList<Query1View>> GetCountOfTasksOfProjectsByUserId(int id)
        {
            try
            {
                IList<Query1View> result = _mapper.Map<IList<Query1DTO>, IList<Query1View>>(_projectService.GetCountOfTasksOfProjectsByUserId(id));

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        // GET: api/<ProjectController>/q2
        [HttpGet("q7")]
        public ActionResult<IList<Query7View>> GetInfoAboutProjects()
        {
            try
            {
                IList<Query7View> result = _mapper.Map<IList<Query7DTO>, IList<Query7View>>(_projectService.GetInfoAboutProjects());

                return Ok(result);
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }
    }
}
