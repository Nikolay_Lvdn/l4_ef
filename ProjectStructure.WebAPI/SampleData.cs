﻿using ProjectStructure.DataAccess;
using ProjectStructure.DataAccess.Entities;
using ProjectStructure.WebAPI.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace ProjectStructure.WebAPI
{
    public class SampleData
    {
        private static Random rnd = new Random();
        public static void InitializeDatabase(CompanyDbContext context)
        {
            InitializeTeams(context);
            InitializeUsers(context);
            InitializeProjects(context);
            InitializeTasks(context);
        }

        private static bool InitializeTeams(CompanyDbContext context)
        {
            if (context.Teams.Any())
            {
                return false;
            }

            context.Teams.AddRange(
                new Team
                {
                    Name = ".Net Team",
                    CreatedAt = DateTime.Now
                },
                new Team
                {
                    Name = "Java Team",
                    CreatedAt = DateTime.Now
                }
                );

            context.SaveChanges();
            return true;
        }

        private static void InitializeUsers(CompanyDbContext context)
        {
            if (context.Users.Any())
            {
                return;
            }

            var teamIds = context.Teams.Select(team => team.Id);

            context.Users.AddRange(
                new User
                {
                    TeamId = teamIds.ToList()[rnd.Next(teamIds.Count())],
                    FirstName = "Nikita",
                    LastName = "Krasnov",
                    Email = "krasnov.n.u@gmail.com",
                    RegisteredAt = DateTime.Now,
                    BirthDay = rnd.RandomDay(new DateTime(1990, 1, 1), DateTime.Now)
                },
                new User
                {
                    TeamId = teamIds.ToList()[rnd.Next(teamIds.Count())],
                    FirstName = "Mykola",
                    LastName = "Levadnyi",
                    Email = "levadnik12@gmail.com",
                    RegisteredAt = DateTime.Now,
                    BirthDay = new DateTime(2002, 1, 12, 7, 0, 0)
                }
                );

            context.SaveChanges();
        }

        private static void InitializeProjects(CompanyDbContext context)
        {
            if (context.Projects.Any())
            {
                return;
            }

            var userIds = context.Users.Select(user => user.Id);
            var teamIds = context.Teams.Select(team => team.Id);

            context.AddRange(
                new Project
                {
                    AuthorId = userIds.ToList()[rnd.Next(userIds.Count())],
                    TeamId = teamIds.ToList()[rnd.Next(teamIds.Count())],
                    Name = "Some Terrific Project",
                    Description = "It's sooooo terrific",
                    Deadline = rnd.RandomDay(DateTime.Now, new DateTime(2030, 1, 1)),
                    CreatedAt = DateTime.Now
                },
                new Project
                {
                    AuthorId = userIds.ToList()[rnd.Next(userIds.Count())],
                    TeamId = teamIds.ToList()[rnd.Next(teamIds.Count())],
                    Name = "Some Dummy Project",
                    Description = "It's sooooo dummy",
                    Deadline = rnd.RandomDay(DateTime.Now, new DateTime(2030, 1, 1)),
                    CreatedAt = DateTime.Now
                }
                );

            context.SaveChanges();
        }

        private static void InitializeTasks(CompanyDbContext context)
        {
            if (context.Tasks.Any())
            {
                return;
            }

            var projectIds = context.Projects.Select(project => project.Id);
            var userIds = context.Users.Select(user => user.Id);

            context.AddRange(
                new Task
                {
                    ProjectId = projectIds.ToList()[rnd.Next(projectIds.Count())],
                    PerformerId = userIds.ToList()[rnd.Next(userIds.Count())],
                    Name = "Do some stuff",
                    Description = "DONT KILL PROD PLEASE",
                    State = 0,
                    CreatedAt = DateTime.Now,
                    FinishedAt = null
                },
                new Task
                {
                    ProjectId = projectIds.ToList()[rnd.Next(projectIds.Count())],
                    PerformerId = userIds.ToList()[rnd.Next(userIds.Count())],
                    Name = "Do some other stuff",
                    Description = "DONT KILL PROD PLEASE",
                    State = 0,
                    CreatedAt = DateTime.Now,
                    FinishedAt = null
                }
                );

            context.SaveChanges();
        }
    }
}
