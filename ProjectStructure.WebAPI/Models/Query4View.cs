﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Models
{
    public class Query4View
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<UserView> Members { get; set; }
        public override string ToString()
        {
            string result = $"Team Id: {Id} \nName: {Name} \n";
            foreach(var member in Members)
            {
                result += $"{member.FirstName} {member.LastName} {member.BirthDay.Date}\n";
            }
            return result;
        }
    }
}
