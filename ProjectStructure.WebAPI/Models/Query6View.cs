﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectStructure.WebAPI.Models
{
    public class Query6View
    {
        public GettingUserView User { get; set; }
        public GettingProjectView LastUserProject { get; set; }
        public int? LastProjectTasksCount { get; set; }
        public int? CountOfIncompleteOrCanceledTasks { get; set; }
        public GettingTaskView LongestTask { get; set; }
        public override string ToString()
        {
            string result = $"User Id: {User.Id} Name:{User.FirstName} {User.LastName}\n";
            result += $"Last User Project\n  Id: {LastUserProject?.Id}\n  Name:{LastUserProject?.Name}\n  Date: {LastUserProject?.CreatedAt}\n";
            result += $"Last Project Tasks Count: {LastProjectTasksCount}\n";
            result += $"Count Of Incomplete Or Canceled Tasks: {CountOfIncompleteOrCanceledTasks}\n";
            result += $"Longest Task\n  Id: {LongestTask?.Id}\n  Name: {LongestTask?.Name}\n";
            return result;
        }
    }
}
