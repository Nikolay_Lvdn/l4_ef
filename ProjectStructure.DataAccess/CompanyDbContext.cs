﻿using Microsoft.EntityFrameworkCore;
using ProjectStructure.DataAccess.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProjectStructure.DataAccess
{
    public class CompanyDbContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Task> Tasks { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        public CompanyDbContext(DbContextOptions<CompanyDbContext> options)
            : base(options) 
        { 
        }
    }
}
